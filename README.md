# OX!

This is OX console game (also known as Tic Tac Toe) written in Java.

## Requirements

* Maven version 3.5.3+
* JDK 1.8+

## Installation

* git clone repository

In console make sure you are in project directory and typ:

> *mvn clean install*

## Run

In project directory type:

> java -jar target/OX-1.0.jar

## Features

* best of three games
* players names and scores
* chose which player starts first
* quit anytime
* configurable winning conditions
* play on any board size from 3 by 3, up to 100 by 100
* square and rectangular boards