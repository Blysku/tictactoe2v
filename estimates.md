# Gra w kółko i krzyżyk

|Data  |Pesymistyczna|Realistyczna|Optymistyczna|
:-------------------:|:-------------------:|:-------------------|-------------------:
|13.11|Przygotowanie projektu, tworzenie planszy o stałych wymiarach| + zmiana graczy, wstawianie znaków do planszy, tworzenie planszy o podanych wymiarach, podstawowa rozgrywka| + walidatory danych wprowadzanych przez użytkownika
|14.11|Zmiana graczy, tworzenie planszy o podanych wymiarach, wstawianie znaków do planszy| + walidacja danych wprowadzanych przez użytkownika| + tworzenie gracza (imię + znak), warunki zwycięstwa
|15.11|Walidatory danych wprowadzanych przez użytkownika| + system oceniania (zwycięstwo, remis)| + system zliczania punktów i określania zwycięzcy całego meczu, warunki zwycięstwa
|16.11|System oceniania (zwycięstwo, remis), warunki zwycięstwa| warunki zwycięstwa | warunki zwycięstwa

Wszystkie funkcjonalności - pokrycie testami > 60%
