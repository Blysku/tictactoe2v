package pl.kk.game.io;

import pl.kk.game.components.Board;
import pl.kk.game.exceptions.BoardToSmallException;
import pl.kk.game.exceptions.ParameterOutOfRangeException;

import java.util.InputMismatchException;

public class Validator {

    private final Board board;

    public Validator(Board board) {
        this.board = board;
    }

    int validateParam(int number, int compensation) throws ParameterOutOfRangeException {
        if (number <= 0 || number > board.getLongerSide()) {
            throw new ParameterOutOfRangeException();
        }
        return number - compensation;
    }

    int validateDimensions(int number) throws ParameterOutOfRangeException {
        if (number < 3 || number > board.getMaxLength()) {
            throw new ParameterOutOfRangeException();
        }
        return number;
    }

    int validateWinSeqLength(int winSeqLength) throws BoardToSmallException {
        if (winSeqLength > this.board.getLongerSide() || winSeqLength < 3) {
            throw new BoardToSmallException();
        }
        return winSeqLength;
    }

    int validateStartingPlayer(String userInput) {
        if (!(userInput = userInput.toLowerCase()).matches("[xo]")){
            throw new InputMismatchException();
        }
        return userInput.charAt(0) == 'x' ? 0 : 1;
    }
}
