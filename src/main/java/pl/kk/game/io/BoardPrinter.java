package pl.kk.game.io;

import pl.kk.game.components.Board;

public class BoardPrinter {

    private final Board board;

    public BoardPrinter(Board board) {
        this.board = board;
    }

    public String stringOfBoard() {
        StringBuilder sb = new StringBuilder();
        for (int row = 0; row < board.getHeight(); row++) {
            for (int col = 0; col < board.getWidth(); col++) {
                sb.append("+---+");
            }
            sb.append("\n");
            for (int col = 0; col < board.getWidth(); col++) {
                sb
                        .append("| ")
                        .append(board.getSign(row, col))
                        .append(" |");
            }
            sb.append("\n");
        }
        for (int col = 0; col < board.getWidth(); col++) {
            sb.append("+---+");
        }
        sb.append("\n");
        return sb.toString();
    }
}

