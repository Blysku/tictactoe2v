package pl.kk.game.io;

import pl.kk.game.exceptions.BoardToSmallException;
import pl.kk.game.exceptions.ParameterOutOfRangeException;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Console {

    private final PrintStream printer;
    private final Scanner reader;
    private final Validator validator;

    public Console(PrintStream printStream, InputStream inputStream, Validator validator) {
        this.printer = new PrintStream(printStream);
        this.reader = new Scanner(inputStream);
        this.validator = validator;
    }

    public Console println(String notification) {
        printer.println(notification);
        return this;
    }

    public Console print(String notification) {
        printer.print(notification);
        return this;
    }

    public int askForWinSeqLength() {
        while (true) {
            try {
                print("> ");
                quitCheck();
                return validator.validateWinSeqLength(reader.nextInt());
            } catch (InputMismatchException e) {
                this.handleWrongInput();
            } catch (BoardToSmallException e) {
                reader.nextLine();
                println("Win sequence length bigger than board! Pick smaller number");
            }
        }
    }

    public int askForParam(int compensation) {
        while (true) {
            try {
                print("> ");
                quitCheck();
                return validator.validateParam(reader.nextInt(), compensation);
            } catch (InputMismatchException e) {
                this.handleWrongInput();
            } catch (ParameterOutOfRangeException e) {
                println("Enter number in range or q to quit");
            }
        }
    }

    public int askForDimensions() {
        while (true) {
            try {
                print("> ");
                quitCheck();
                return validator.validateDimensions(reader.nextInt());
            } catch (InputMismatchException e) {
                this.handleWrongInput();
            } catch (ParameterOutOfRangeException e) {
                println("Enter number in range or q to quit");
            }
        }
    }

    public int askWhoStarts() {
        while (true) {
            try {
                print("> ");
                quitCheck();
                return validator.validateStartingPlayer(reader.next());
            } catch (InputMismatchException e) {
                println("Illegal character! Enter X or O");
            }
        }
    }

    public String askForName() {
        print("> ");
        quitCheck();
        return reader.nextLine();
    }

    private void handleWrongInput() {
        reader.nextLine();
        println("Enter number in range or q to quit");
    }

    private void quitCheck() {
        if (reader.hasNext("q")) {
            printer.print("Good Bye!");
            System.exit(0);
        }
    }
}
