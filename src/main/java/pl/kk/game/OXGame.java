package pl.kk.game;

import pl.kk.game.components.Board;
import pl.kk.game.io.BoardPrinter;
import pl.kk.game.io.Console;
import pl.kk.game.io.Validator;
import pl.kk.game.states.GameState;
import pl.kk.game.states.InitialState;

class OXGame {

    private GameState state;

    private OXGame(int minBoardLength, int maxBoardLength) {
        Board board = new Board(minBoardLength, maxBoardLength);
        Console console = new Console(System.out, System.in, new Validator(board));
        this.state = new InitialState(console, new BoardPrinter(board), board);
    }

    public static void main(String[] args) {
        OXGame game = new OXGame(3, 100);
        game.run();
    }

    private void run() {
        do {
            this.state = this.state.next();
        } while (!state.isGameWon());
    }
}
