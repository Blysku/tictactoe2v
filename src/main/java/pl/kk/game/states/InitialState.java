package pl.kk.game.states;

import pl.kk.game.components.Players;
import pl.kk.game.components.Board;
import pl.kk.game.io.BoardPrinter;
import pl.kk.game.io.Console;
import pl.kk.game.states.round.StartRoundState;

import java.util.Arrays;

public class InitialState implements GameState {

    private final Console console;
    private final BoardPrinter boardPrinter;
    private final Players players;
    private final Board board;

    public InitialState(Console console, BoardPrinter boardPrinter, Board board) {
        this.console = console;
        this.boardPrinter = boardPrinter;
        this.board = board;
        this.players = new Players(2, Arrays.asList('X', 'O'));
    }

    @Override
    public GameState next() {
        this.console.println("Welcome to OX game");
        this.console.println("Type q to quit anytime");
        String name = this.console.println("Please enter players X name")
                .askForName();
        this.players.addPlayer(name);
        name = this.console.println("Please enter players O name")
                .askForName();
        this.players.addPlayer(name);
        int firstPlayer = this.console.println("Who starts first? X or O?")
                .askWhoStarts();
        this.players.setCurrentPlayer(firstPlayer);
        return new StartRoundState(players, this.console, this.board, boardPrinter, 1);
    }

    @Override
    public boolean isGameWon() {
        return false;
    }


}
