package pl.kk.game.states;

import pl.kk.game.components.Player;
import pl.kk.game.components.Players;
import pl.kk.game.io.Console;

public class VictoryState implements GameState {
    private final Players players;
    private final Console console;

    public VictoryState(Players players, Console console) {
        this.players = players;
        this.console = console;
    }

    @Override
    public GameState next() {
        Player playerX = players.getPlayers().get(0);
        Player playerO = players.getPlayers().get(1);
        if (playerX.getScore().equals(playerO.getScore())){
            console.println("It is a draw!");
        } else {
            Player winner = playerX.getScore().getScore() > playerO.getScore().getScore()
                    ? playerX : playerO;
            console.println("Congratulations " + winner.getName() + ", you have won the game!");
        }
        return this;
    }

    @Override
    public boolean isGameWon() {
        return true;
    }
}
