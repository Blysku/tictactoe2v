package pl.kk.game.states;

import pl.kk.game.io.Console;

public class ErrorState implements GameState {

    private final String errorMessage;
    private final Console console;
    private final GameState lastState;

    public ErrorState(String errorMessage, Console console, GameState lastState) {
        this.errorMessage = errorMessage;
        this.console = console;
        this.lastState = lastState;
    }

    @Override
    public GameState next() {
        this.console.println(errorMessage);
        return lastState;
    }

    @Override
    public boolean isGameWon() {
        return false;
    }
}
