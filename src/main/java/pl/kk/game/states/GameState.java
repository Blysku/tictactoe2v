package pl.kk.game.states;

public interface GameState {

    GameState next();

    boolean isGameWon();
}
