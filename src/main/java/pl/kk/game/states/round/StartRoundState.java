package pl.kk.game.states.round;

import pl.kk.game.components.Players;
import pl.kk.game.components.Board;
import pl.kk.game.exceptions.BoardToSmallException;
import pl.kk.game.io.BoardPrinter;
import pl.kk.game.io.Console;
import pl.kk.game.states.GameState;

public class StartRoundState extends RoundState {

    public StartRoundState(Players players, Console console, Board board, BoardPrinter boardPrinter, int currentRound) {
        super(players, console, board, boardPrinter, currentRound);
        this.currentRound = currentRound;
        this.console = console;
        this.players = players;
        this.boardPrinter = boardPrinter;
    }

    @Override
    public GameState next() {
        console.println("Please set up new board");
        int height = this.console.println("Enter board height up to " + board.getMaxLength())
                .askForDimensions();
        int width = this.console.println("Enter board width up to " + board.getMaxLength())
                .askForDimensions();
        try {
            board.setUp(height, width);
        } catch (BoardToSmallException e) {
            console.println("Board size to small, try again");
            return this;
        }
        int winSeqLength = this.console.println("Enter sequence length for the win")
                .askForWinSeqLength();
        console.print(boardPrinter.stringOfBoard());
        return new PlayRoundState(players, console, board, boardPrinter, currentRound, winSeqLength, 1);
    }

    @Override
    public boolean isGameWon() {
        return false;
    }
}
