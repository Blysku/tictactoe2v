package pl.kk.game.states.round;

import pl.kk.game.components.Board;
import pl.kk.game.components.Player;
import pl.kk.game.components.Players;
import pl.kk.game.io.BoardPrinter;
import pl.kk.game.io.Console;
import pl.kk.game.states.GameState;
import pl.kk.game.states.VictoryState;

public class RoundDrawState extends RoundState {

    RoundDrawState(Players players, Console console, Board board, BoardPrinter boardPrinter, int currentRound) {
        super(players, console, board, boardPrinter, currentRound);
    }

    @Override
    public GameState next() {
        console.print("Round " + currentRound + " is a draw");
        console.println("Scores");
        for (Player player : players.getPlayers()) {
            console.println(player.getName() + "(" + player.getSymbol() + "): " + player.getScore());
        }
        if (currentRound == roundsToWin) {
            return new VictoryState(players, console);
        } else {
            return new StartRoundState(players, console, board, boardPrinter, currentRound + 1);
        }
    }

    @Override
    public boolean isGameWon() {
        return false;
    }
}
