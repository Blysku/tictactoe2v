package pl.kk.game.states.round;

import pl.kk.game.components.Move;
import pl.kk.game.components.Players;
import pl.kk.game.VictoryCheck;
import pl.kk.game.components.Board;
import pl.kk.game.io.BoardPrinter;
import pl.kk.game.io.Console;
import pl.kk.game.states.GameState;

abstract class RoundState implements GameState {

    Players players;
    Console console;
    final Board board;
    BoardPrinter boardPrinter;
    int currentRound;
    int movesCounter;
    final int roundsToWin = 3;
    private final VictoryCheck victoryCheck = new VictoryCheck();


    RoundState(Players players, Console console, Board board, BoardPrinter boardPrinter, int currentRound) {
        this.players = players;
        this.console = console;
        this.board = board;
        this.boardPrinter = boardPrinter;
        this.currentRound = currentRound;
    }

    boolean isRoundWon(Move lastMove, int winSeq) {
        return victoryCheck.isRoundWon(board, lastMove, winSeq);
    }

    boolean isDraw() {
        return victoryCheck.isDraw(board);
    }

}