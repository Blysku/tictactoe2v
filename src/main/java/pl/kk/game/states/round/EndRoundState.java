package pl.kk.game.states.round;

import pl.kk.game.components.Player;
import pl.kk.game.components.Players;
import pl.kk.game.components.Board;
import pl.kk.game.io.BoardPrinter;
import pl.kk.game.io.Console;
import pl.kk.game.states.GameState;
import pl.kk.game.states.VictoryState;

public class EndRoundState extends RoundState {

    EndRoundState(Players players, Console console, Board board, BoardPrinter boardPrinter, int currentRound) {
        super(players, console, board, boardPrinter, currentRound);
        this.currentRound = currentRound;
        this.console = console;
        this.players = players;
        this.boardPrinter = boardPrinter;
    }

    @Override
    public GameState next() {
        console.println(players.getCurrentPlayer().getName() + " won round " + currentRound);
        console.println("Scores");
        for (Player player : players.getPlayers()) {
            console.println(player.getName() + "(" + player.getSymbol() + "): " + player.getScore());
        }
        if (currentRound == roundsToWin) {
            return new VictoryState(players, console);
        } else {
            return new StartRoundState(players, console, board, boardPrinter, currentRound + 1);
        }
    }

    @Override
    public boolean isGameWon() {
        return false;
    }
}
