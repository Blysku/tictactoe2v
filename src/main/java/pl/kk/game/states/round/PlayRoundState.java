package pl.kk.game.states.round;


import pl.kk.game.components.Move;
import pl.kk.game.components.Players;
import pl.kk.game.components.Board;
import pl.kk.game.exceptions.CoordinatesOutsideTheBoardException;
import pl.kk.game.exceptions.FieldOccupiedException;
import pl.kk.game.io.BoardPrinter;
import pl.kk.game.io.Console;
import pl.kk.game.states.ErrorState;
import pl.kk.game.states.GameState;

public class PlayRoundState extends RoundState {

    private final int winSeqLength;

    PlayRoundState(Players players, Console console, Board board, BoardPrinter boardPrinter, int currentRound, int winSeqLength, int movesCounter) {
        super(players, console, board, boardPrinter, currentRound);
        this.players = players;
        this.console = console;
        this.boardPrinter = boardPrinter;
        this.currentRound = currentRound;
        this.winSeqLength = winSeqLength;
        this.movesCounter = movesCounter;
    }

    @Override
    public GameState next() {
        int row = console.println(this.players.getCurrentPlayer().getName() + " enter row number")
                .askForParam(1);
        int col = console.println(this.players.getCurrentPlayer().getName() + " enter column number")
                .askForParam(1);
        Move lastMove;
        try {
            lastMove = board.put(this.players.getCurrentPlayer(), row, col);
            movesCounter++;
            console.print(boardPrinter.stringOfBoard());
            if (isRoundWon(lastMove, winSeqLength)) {
                players.getCurrentPlayer().increaseScore(3);
                return new EndRoundState(players, console, board, boardPrinter, currentRound);
            } else if (isDraw()) {
                players.getCurrentPlayer().increaseScore(1);
                players.getAnotherPlayer().increaseScore(1);
                return new RoundDrawState(players, console, board, boardPrinter, currentRound);
            } else {
                this.players.nextPlayer();
                return this;
            }
        } catch (CoordinatesOutsideTheBoardException e) {
            return new ErrorState("Coordinates outside the board! Try again", this.console, this);
        } catch (FieldOccupiedException e) {
            return new ErrorState("Field occupied! Try again", this.console, this);
        }
    }

    @Override
    public boolean isGameWon() {
        return false;
    }


}
