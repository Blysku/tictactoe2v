package pl.kk.game.components;

import java.util.Objects;

public class Score {

    private int score;

    Score() {
        this.score = 0;
    }

    Score(int score) {
        this.score = score;
    }

    void add(int value) {
        this.score += value;
    }

    public int getScore() {
        return score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Score score1 = (Score) o;
        return score == score1.score;
    }

    @Override
    public int hashCode() {
        return Objects.hash(score);
    }

    @Override
    public String toString() {
        return String.valueOf(score);
    }
}
