package pl.kk.game.components;

public class Move {

    private final Player currentPlayer;
    private final int row;
    private final int col;

    public Move(Player currentPlayer, int row, int col) {
        this.currentPlayer = currentPlayer;
        this.row = row;
        this.col = col;
    }

    public Character getSign() {
        return currentPlayer.getSymbol();
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }
}
