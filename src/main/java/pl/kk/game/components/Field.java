package pl.kk.game.components;

import pl.kk.game.exceptions.FieldOccupiedException;

public class Field {

    private Character symbol;

    Field() {
        this.symbol = ' ';
    }

    void put(Character symbol) throws FieldOccupiedException {
        if (' ' != this.symbol) {
            throw new FieldOccupiedException("Field occupied");
        }
        this.symbol = symbol;
    }

    Character getSymbol() {
        return symbol;
    }

    @Override
    public String toString() {
        return String.valueOf(symbol);
    }
}
