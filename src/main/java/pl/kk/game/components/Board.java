package pl.kk.game.components;

import pl.kk.game.exceptions.BoardToSmallException;
import pl.kk.game.exceptions.FieldOccupiedException;
import pl.kk.game.exceptions.CoordinatesOutsideTheBoardException;


public class Board {

    private Field[][] fields;
    private final int minLength;
    private final int maxLength;

    public Board(int minLength, int maxLength) {
        this.minLength = minLength;
        this.maxLength = maxLength;
    }

    public int getHeight() {
        return this.fields.length;
    }

    public int getWidth() {
        return this.fields[0].length;
    }

    public int getMaxLength() {
        return maxLength;
    }


    public Field getField(int index) throws CoordinatesOutsideTheBoardException {
        if (index < 0) throw new CoordinatesOutsideTheBoardException();
        if (index < fields[0].length) return fields[0][index];
        for (int i = 1; i < fields.length; i++) {
            index -= fields.length;
            if (index < fields[i].length) return fields[i][index];
        }
        throw new CoordinatesOutsideTheBoardException();
    }

    public int getLongerSide() {
        return getHeight() > getWidth() ? getHeight() : getWidth();
    }

    public Move put(Player currentPlayer, int row, int col) throws FieldOccupiedException, CoordinatesOutsideTheBoardException {
        if (row >= this.getHeight() || col >= this.getWidth()) {
            throw new CoordinatesOutsideTheBoardException("row: " + row + " column: " + col);
        }
        fields[row][col].put(currentPlayer.getSymbol());
        return new Move(currentPlayer, row, col);
    }

    public Move put(Player currentPlayer, int index) throws FieldOccupiedException, CoordinatesOutsideTheBoardException {
        if (index < fields[0].length) return put(currentPlayer, 0, index);
        else {
            for (int i = 1; i < fields.length; i++) {
                index -= fields.length;
                if (index < fields[i].length) return put(currentPlayer, i, index);
            }
        }
        throw new CoordinatesOutsideTheBoardException();
    }

    public Character getSign(int row, int col) {
        return fields[row][col].getSymbol();
    }

    public boolean checkSign(int row, int col, Character symbol) {
        return fields[row][col].getSymbol().equals(symbol);
    }

    public boolean setUp(int rows, int cols) throws BoardToSmallException {
        if (rows < minLength || cols < minLength) {
            throw new BoardToSmallException();
        }
        this.fields = new Field[rows][cols];
        this.initializeFields();
        return true;
    }

    private void initializeFields() {
        for (int row = 0; row < fields.length; row++) {
            for (int col = 0; col < fields[row].length; col++) {
                fields[row][col] = new Field();
            }
        }
    }
}

