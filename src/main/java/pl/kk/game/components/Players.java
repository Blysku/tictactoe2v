package pl.kk.game.components;

import java.util.ArrayList;
import java.util.List;

public class Players {

    private final List<Player> players;
    private int currentPlayer;
    private final List<Character> symbols;
    private int nextSymbol;
    private int size;

    public Players(int quantity, List<Character> symbols) {
        this.players = new ArrayList<>(quantity);
        this.symbols = symbols;
        this.nextSymbol = -1;
        this.currentPlayer = 0;
        this.size = 0;
    }

    public void nextPlayer() {
        if (this.currentPlayer == size - 1) {
            this.currentPlayer = 0;
        } else {
            this.currentPlayer++;
        }
    }

    public void setCurrentPlayer(int index) {
        currentPlayer = index;
    }

    public Player getCurrentPlayer() {
        return this.players.get(currentPlayer);
    }

    public Player getAnotherPlayer(){
        int index = currentPlayer == 0 ? 1 : 0;
        return players.get(index);
    }

    public List<Player> getPlayers() {
        return players;
    }

    public boolean addPlayer(String name) {
        this.changeSymbol();
        players.add(new Player(name, symbols.get(nextSymbol)));
        size++;
        return true;
    }

    private void changeSymbol() {
        nextSymbol++;
    }
}
