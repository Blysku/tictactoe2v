package pl.kk.game.components;

public class Player {

    private final String name;
    private final Character symbol;
    private Score score;

    public Player(String name, Character symbol) {
        this.name = name;
        this.symbol = symbol;
        this.score = new Score();
    }

    public Character getSymbol() {
        return symbol;
    }

    public String getName() {
        return name;
    }

    public Score getScore() {
        return score;
    }

    @Override
    public String toString() {
        return name;
    }

    public void increaseScore(int value) {
        score.add(value);
    }
}
