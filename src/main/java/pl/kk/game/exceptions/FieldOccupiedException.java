package pl.kk.game.exceptions;

public class FieldOccupiedException extends Exception {

    public FieldOccupiedException(String message) {
        super(message);
    }
}
