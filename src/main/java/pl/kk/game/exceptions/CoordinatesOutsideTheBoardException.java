package pl.kk.game.exceptions;

public class CoordinatesOutsideTheBoardException extends Exception {

    public CoordinatesOutsideTheBoardException() {
    }

    public CoordinatesOutsideTheBoardException(String message) {
        super(message);
    }
}
