package pl.kk.game;

import pl.kk.game.components.Board;
import pl.kk.game.components.Move;

public class VictoryCheck {

    public boolean isRoundWon(Board board, Move lastMove, int winSeq) {
        if (checkVerticalWin(board, lastMove, winSeq)) return true;
        if (checkHorizontalWin(board, lastMove, winSeq)) return true;
        if (checkDiagonalWin(board, lastMove, winSeq)) return true;
        return checkReverseDiagonalWin(board, lastMove, winSeq);
    }


    public boolean isDraw(Board board) {
        for (int col = 0; col < board.getWidth(); col ++){
            for (int row = 0; row < board.getHeight(); row++) {
                if (board.getSign(row,col).equals(' ')){
                    return false;
                }
            }
        }
        return true;
    }

    private boolean checkVerticalWin(Board board, Move lastMove, int winSeq) {
        int currentSequence = 0;
        for (int i = 0; i <= winSeq * 2; i++) {
            int rowCheckPosition = lastMove.getRow() - winSeq - 1 + i;

            if (rowCheckPosition >= 0 && rowCheckPosition < board.getHeight()
                    && board.checkSign(rowCheckPosition, lastMove.getCol(), lastMove.getSign())) {
                currentSequence++;
            } else {
                currentSequence = 0;
            }
            if (currentSequence == winSeq) {
                return true;
            }
        }
        return false;
    }

    private boolean checkHorizontalWin(Board board, Move lastMove, int winSeq) {
        int currentSequence = 0;
        for (int i = 0; i <= winSeq * 2; i++) {
            int columnCheckPosition = lastMove.getCol() - winSeq - 1 + i;

            if (columnCheckPosition >= 0 && columnCheckPosition < board.getWidth()
                    && board.checkSign(lastMove.getRow(), columnCheckPosition, lastMove.getSign())) {
                currentSequence++;
            } else {
                currentSequence = 0;
            }
            if (currentSequence == winSeq) {
                return true;
            }
        }
        return false;
    }

    private boolean checkDiagonalWin(Board board, Move lastMove, int winSeq) {
        int currentSequence = 0;
        for (int i = 0; i <= winSeq * 2; i++) {
            int columnCheckPosition = lastMove.getCol() - winSeq - 1 + i;
            int rowCheckPosition = lastMove.getRow() - winSeq - 1 + i;

            if (columnCheckPosition >= 0 && columnCheckPosition < board.getWidth()
                    && rowCheckPosition >= 0 && rowCheckPosition < board.getHeight()
                    && board.checkSign(rowCheckPosition, columnCheckPosition, lastMove.getSign())) {
                currentSequence++;
            } else {
                currentSequence = 0;
            }
            if (currentSequence == winSeq) {
                return true;
            }
        }
        return false;
    }

    private boolean checkReverseDiagonalWin(Board board, Move lastMove, int winSeq) {
        int currentSequence = 0;
        for (int i = 0; i <= winSeq * 2; i++) {
            int rowCheckPosition = lastMove.getRow() - winSeq + 1 + i;
            int columnCheckPosition = lastMove.getCol() + winSeq - 1 - i;

            if (columnCheckPosition >= 0 && columnCheckPosition < board.getWidth()
                    && rowCheckPosition >= 0 && rowCheckPosition < board.getHeight()
                    && board.checkSign(rowCheckPosition, columnCheckPosition, lastMove.getSign())) {
                currentSequence++;
            } else {
                currentSequence = 0;
            }
            if (currentSequence == winSeq) {
                return true;
            }
        }
        return false;
    }
}
