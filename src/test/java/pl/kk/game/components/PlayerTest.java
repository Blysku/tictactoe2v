package pl.kk.game.components;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class PlayerTest {

    @Test
    public void shouldIncreasePlayersScore() {
        Player player = new Player("Bob", 'X');
        Score score = new Score(0);
        assertEquals(player.getScore(), score);
        player.increaseScore(3);
        score = new Score(3);
        assertEquals(player.getScore(), score);
    }
}