package pl.kk.game.components;

import org.testng.annotations.Test;

import java.util.Arrays;

import static org.testng.Assert.*;

public class PlayersTest {


    @Test
    public void shouldReturnNextPlayer() {
        Character x = 'X';
        Character o = 'O';
        Players players = new Players(2, Arrays.asList(x, o));
        String firstPlayerName = "Bob";
        String secondPlayerName = "Jerry";

        players.addPlayer(firstPlayerName);
        players.addPlayer(secondPlayerName);

        players.nextPlayer();
        assertEquals(players.getCurrentPlayer().getName(), secondPlayerName);
        players.nextPlayer();
        assertEquals(players.getCurrentPlayer().getName(), firstPlayerName);
    }

    @Test
    public void shouldSetPlayerAsCurrentPlayer() {
        Character x = 'X';
        Character o = 'O';
        Players players = new Players(2, Arrays.asList(x, o));
        String firstPlayerName = "Bob";
        String secondPlayerName = "Jerry";

        players.addPlayer(firstPlayerName);
        players.addPlayer(secondPlayerName);
        players.setCurrentPlayer(1);

        assertEquals(players.getCurrentPlayer().getName(), secondPlayerName);
    }

    @Test
    public void shouldAddPlayerWithNameAndSign() {
        Character x = 'X';
        Character o = 'O';
        Players players = new Players(2, Arrays.asList(x, o));
        String firstPlayerName = "Bob";

        players.addPlayer(firstPlayerName);
        players.addPlayer(firstPlayerName);

        assertEquals(players.getPlayers().get(0).getName(), firstPlayerName);
        assertEquals(players.getPlayers().get(0).getSymbol(), x);
        assertEquals(players.getPlayers().get(1).getSymbol(), o);
    }
}