package pl.kk.game.components;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class BoardTest {

    private static final int BOARD_HEIGHT = 3;
    private static final int BOARD_WIDTH = 3;
    private static final int MIN_BOARD_SIZE = 3;
    private static final int MAX_BOARD_SIZE = 100;
    private static final char SIGN = 'X';

    @Test
    public void shouldPlaceSignOnGivenCoorinatesOnTheBoard() throws Exception {
        Board board = new Board(MIN_BOARD_SIZE, MAX_BOARD_SIZE);
        board.setUp(BOARD_HEIGHT, BOARD_WIDTH);
        board.put(new Player("X",'X'), 1, 1);
        assertEquals(board.getSign(1, 1), Character.valueOf('X'));
    }

    @Test
    public void shouldGetFieldWork() throws Exception {
        Board board = new Board(MIN_BOARD_SIZE, MAX_BOARD_SIZE);
        board.setUp(BOARD_HEIGHT, BOARD_WIDTH);
        Player playerX = new Player(String.valueOf(SIGN), SIGN);

        board.put(playerX, 2, 2);
        board.put(playerX, 1, 2);

        assertEquals(board.getField(5).toString(), "X");
        assertEquals(board.getField(8).toString(), "X");
    }

    @Test
    public void shouldPutWorkForSingleIndex() throws Exception {
        Board board = new Board(MIN_BOARD_SIZE, MAX_BOARD_SIZE);
        board.setUp(BOARD_HEIGHT, BOARD_WIDTH);
        Player playerX = new Player(String.valueOf(SIGN), SIGN);

        board.put(playerX, 5);
        board.put(playerX, 8);

        assertEquals(board.getSign(1,2).toString(), "X");
        assertEquals(board.getSign(2,2).toString(), "X");
    }


}