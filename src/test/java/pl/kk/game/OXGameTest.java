package pl.kk.game;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pl.kk.game.components.Board;
import pl.kk.game.components.Move;
import pl.kk.game.components.Player;
import pl.kk.game.exceptions.BoardToSmallException;
import pl.kk.game.exceptions.CoordinatesOutsideTheBoardException;
import pl.kk.game.exceptions.FieldOccupiedException;
import pl.kk.game.io.BoardPrinter;

import java.util.stream.IntStream;

import static org.testng.Assert.*;

public class OXGameTest {

    private static final int BOARD_HEIGHT = 3;
    private static final int BOARD_WIDTH = 3;
    private static final int MIN_BOARD_SIZE = 3;
    private static final int MAX_BOARD_SIZE = 100;
    private static final char SIGN = 'X';
    private static final char WIN_SEQUENCE = 3;

    @DataProvider
    private Object[][] paramGeneratorFor5by5BoardAndWinSequence3Diagonal() {
        int boardSize = 5;
        int localWinSeq = 3;
        Object[][] objects = new Object[boardSize * localWinSeq + localWinSeq][localWinSeq];
        int counter = 0;
        for (int j = 0; j < boardSize - localWinSeq + 1; j++) {
            for (int i = 0; i <= boardSize - localWinSeq; i++) {
                int mark;
                objects[counter] = IntStream.iterate(i + (j * boardSize), anyInt -> anyInt + boardSize + 1).limit(localWinSeq).boxed().toArray();
                counter++;
            }
        }
        for (int j = 1; j <= boardSize - localWinSeq + 1; j++) {
            for (int i = 0; i <= boardSize - localWinSeq; i++) {
                objects[counter++] = IntStream.iterate(j * boardSize - i - 1, anyInt -> anyInt + boardSize - 1).limit(localWinSeq).boxed().toArray();
            }
        }
        return objects;
    }

    @Test(dataProvider = "paramGeneratorFor5by5BoardAndWinSequence3Diagonal")
    public void shouldWinHorizontally5by5BoardAndWinSequence3Diagonal(int[] index) throws Exception {
        winCheck(index);
    }

    private void winCheck(int[] index) throws BoardToSmallException, FieldOccupiedException, CoordinatesOutsideTheBoardException {
        int boardSize = 5;
        int localWinSeq = 3;
        VictoryCheck victoryCheck = new VictoryCheck();
        Board board = new Board(MIN_BOARD_SIZE, MAX_BOARD_SIZE);
        board.setUp(boardSize, boardSize);
        Player playerX = new Player(String.valueOf(SIGN), SIGN);
        BoardPrinter boardPrinter = new BoardPrinter(board);
        Move lastMove = null;

        for (int j = 0; j < localWinSeq; j++) {
            lastMove = board.put(playerX, index[j]);
            System.out.println(boardPrinter.stringOfBoard());
        }

        assertTrue(victoryCheck.isRoundWon(board, lastMove, localWinSeq));
    }

    @DataProvider
    private Object[][] paramGeneratorFor5by5BoardAndWinSequence3HorizontalAndVertical() {
        int boardSize = 5;
        int localWinSeq = 3;
        Object[][] objects = new Object[boardSize * (boardSize - localWinSeq + 1) * 2][localWinSeq];
        int counter = 0;
        for (int j = 0; j < boardSize - localWinSeq + 1; j++) {
            for (int i = 0; i <= boardSize; i++) {
                counter = i + (j * boardSize);
                objects[counter] = IntStream.iterate(i * boardSize + j, anyInt -> anyInt + 1).limit(localWinSeq).boxed().toArray();
            }
        }
        for (int i = 0; i < boardSize * (boardSize - localWinSeq + 1); i++) {
            objects[counter++] = IntStream.iterate(i, anyInt -> anyInt + boardSize).limit(localWinSeq).boxed().toArray();
        }
        return objects;
    }

    @Test(dataProvider = "paramGeneratorFor5by5BoardAndWinSequence3HorizontalAndVertical")
    public void shouldWinHorizontally5by5BoardAndWinSequence3HorizontalAndVertical(int[] index) throws Exception {
        winCheck(index);
    }

    @DataProvider
    private Object[][] paramGeneratorFor5by5BoardAndWinSequence5() {
        int boardSize = 5;
        int localWinSeq = 5;
        Object[][] objects = new Object[boardSize * 2][localWinSeq];
        for (int i = 0; i < boardSize; i++) {
            objects[i] = IntStream.iterate(i * boardSize, anyInt -> anyInt + 1).limit(localWinSeq).boxed().toArray();
        }
        for (int i = 0; i < boardSize; i++) {
            objects[i + boardSize] = IntStream.iterate(i, anyInt -> anyInt + boardSize).limit(localWinSeq).boxed().toArray();
        }
        return objects;
    }

    @Test(dataProvider = "paramGeneratorFor5by5BoardAndWinSequence5")
    public void shouldWinHorizontally5by5BoardAndWinSequence5(int[] index) throws Exception {
        int boardSize = 5;
        int localWinSeq = 5;
        VictoryCheck victoryCheck = new VictoryCheck();
        Board board = new Board(MIN_BOARD_SIZE, MAX_BOARD_SIZE);
        board.setUp(boardSize, boardSize);
        Player playerX = new Player(String.valueOf(SIGN), SIGN);
        BoardPrinter boardPrinter = new BoardPrinter(board);
        Move lastMove = null;

        for (int j = 0; j < localWinSeq; j++) {
            lastMove = board.put(playerX, index[j]);
            System.out.println(boardPrinter.stringOfBoard());
        }

        assertTrue(victoryCheck.isRoundWon(board, lastMove, localWinSeq));
    }

    @DataProvider
    private Object[][] paramGeneratorFor3by3BoardAndWinSequence3() {
        int boardSize = 3;
        int localWinSeq = 3;
        Object[][] objects = new Object[boardSize * 2 + 4][localWinSeq];

        for (int i = 0; i < boardSize; i++) {
            objects[i] = IntStream.iterate(i * boardSize, anyInt -> anyInt + 1).limit(localWinSeq).boxed().toArray();
        }
        for (int i = 0; i < boardSize; i++) {
            objects[i + boardSize] = IntStream.iterate(i, anyInt -> anyInt + boardSize).limit(localWinSeq).boxed().toArray();
        }
        objects[boardSize * 2] = IntStream.iterate(0, anyInt -> anyInt + boardSize + 1).limit(localWinSeq).boxed().toArray();
        objects[boardSize * 2 + 1] = IntStream.iterate(8, anyInt -> anyInt - boardSize - 1).limit(localWinSeq).boxed().toArray();
        objects[boardSize * 2 + 2] = IntStream.iterate(2, anyInt -> anyInt + boardSize - 1).limit(localWinSeq).boxed().toArray();
        objects[boardSize * 2 + 3] = IntStream.iterate(6, anyInt -> anyInt - boardSize + 1).limit(localWinSeq).boxed().toArray();

        return objects;
    }


    @Test(dataProvider = "paramGeneratorFor3by3BoardAndWinSequence3")
    public void shouldWinHorizontally3by3BoardAndWinSequence3(int[] index) throws Exception {
        int boardSize = 3;
        int localWinSeq = 3;
        VictoryCheck victoryCheck = new VictoryCheck();
        Board board = new Board(MIN_BOARD_SIZE, MAX_BOARD_SIZE);
        board.setUp(boardSize, boardSize);
        Player playerX = new Player(String.valueOf(SIGN), SIGN);
        BoardPrinter boardPrinter = new BoardPrinter(board);
        Move lastMove = null;

        for (int j = 0; j < localWinSeq; j++) {
            lastMove = board.put(playerX, index[j]);
            System.out.println(boardPrinter.stringOfBoard());
        }

        assertTrue(victoryCheck.isRoundWon(board, lastMove, localWinSeq));
    }

    @DataProvider
    private Object[][] reverseDiagonalWinParams() {
        int shorterSide = BOARD_HEIGHT < BOARD_WIDTH ? BOARD_HEIGHT : BOARD_WIDTH;
        int numOfTests = shorterSide - WIN_SEQUENCE + 1;

        Object[][] objects = new Object[numOfTests][2];
        for (int i = 0; i < numOfTests; i++) {
            int[] columns = new int[WIN_SEQUENCE];
            int[] rows = new int[WIN_SEQUENCE];
            for (int j = 0; j < WIN_SEQUENCE; j++) {
                rows[j] = j + i;
                columns[j] = BOARD_WIDTH - 1 - j - i;
            }
            objects[i][0] = rows;
            objects[i][1] = columns;
        }
        return objects;
    }

    @Test(dataProvider = "reverseDiagonalWinParams")
    public void shouldWinReverseDiagonally(int[] rows, int[] columns) throws Exception {
        VictoryCheck victoryCheck = new VictoryCheck();
        Board board = new Board(MIN_BOARD_SIZE, MAX_BOARD_SIZE);
        board.setUp(BOARD_HEIGHT, BOARD_WIDTH);
        Player playerX = new Player(String.valueOf(SIGN), SIGN);
        BoardPrinter boardPrinter = new BoardPrinter(board);

        Move lastMove = null;
        for (int j = 0; j < WIN_SEQUENCE; j++) {
            lastMove = board.put(playerX, rows[j], columns[j]);

            System.out.println(boardPrinter.stringOfBoard());
        }
        assertTrue(victoryCheck.isRoundWon(board, lastMove, WIN_SEQUENCE));
    }

    @DataProvider
    private Object[][] diagonalWinParams() {
        int shorterSide = BOARD_HEIGHT < BOARD_WIDTH ? BOARD_HEIGHT : BOARD_WIDTH;
        int numOfTests = shorterSide - WIN_SEQUENCE + 1;

        Object[][] objects = new Object[numOfTests][2];
        for (int i = 0; i < numOfTests; i++) {
            int[] columns = new int[WIN_SEQUENCE];
            int[] rows = new int[WIN_SEQUENCE];
            for (int j = 0; j < WIN_SEQUENCE; j++) {
                rows[j] = j + i;
                columns[j] = j + i;
            }
            objects[i][0] = rows;
            objects[i][1] = columns;
        }
        return objects;
    }

    @Test(dataProvider = "diagonalWinParams")
    public void shouldWinDiagonally(int[] rows, int[] columns) throws Exception {
        VictoryCheck victoryCheck = new VictoryCheck();
        Board board = new Board(MIN_BOARD_SIZE, MAX_BOARD_SIZE);
        board.setUp(BOARD_HEIGHT, BOARD_WIDTH);
        Player playerX = new Player(String.valueOf(SIGN), SIGN);
        BoardPrinter boardPrinter = new BoardPrinter(board);

        Move lastMove = null;
        for (int j = 0; j < WIN_SEQUENCE; j++) {
            lastMove = board.put(playerX, rows[j], columns[j]);

            System.out.println(boardPrinter.stringOfBoard());
        }
        assertTrue(victoryCheck.isRoundWon(board, lastMove, WIN_SEQUENCE));
    }

    @DataProvider
    private Object[][] horizontalWinParams() {
        int trueWidth = BOARD_WIDTH - WIN_SEQUENCE + 1;
        int numOfTests = BOARD_HEIGHT * (BOARD_HEIGHT - WIN_SEQUENCE + 1);

        Object[][] objects = new Object[numOfTests][2];
        for (int k = 0; k < trueWidth; k++) {
            for (int i = 0; i < BOARD_HEIGHT; i++) {
                int[] columns = new int[WIN_SEQUENCE];
                int[] rows = new int[WIN_SEQUENCE];
                for (int j = 0; j < WIN_SEQUENCE; j++) {
                    rows[j] = i;
                    columns[j] = k + j;
                }
                objects[i + (BOARD_HEIGHT * k)][0] = rows;
                objects[i + (BOARD_HEIGHT * k)][1] = columns;
            }
        }
        return objects;
    }

    @Test(dataProvider = "horizontalWinParams")
    public void shouldWinHorizontally(int[] rows, int[] columns) throws BoardToSmallException, FieldOccupiedException, CoordinatesOutsideTheBoardException {
        VictoryCheck victoryCheck = new VictoryCheck();
        Board board = new Board(MIN_BOARD_SIZE, MAX_BOARD_SIZE);
        board.setUp(BOARD_HEIGHT, BOARD_WIDTH);
        Player playerX = new Player(String.valueOf(SIGN), SIGN);
        BoardPrinter boardPrinter = new BoardPrinter(board);

        Move lastMove = null;
        for (int j = 0; j < WIN_SEQUENCE; j++) {
            lastMove = board.put(playerX, rows[j], columns[j]);
            System.out.println(boardPrinter.stringOfBoard());
        }
        assertTrue(victoryCheck.isRoundWon(board, lastMove, WIN_SEQUENCE));
    }

    @DataProvider
    private Object[][] verticalWinParamsExtended() {
        int trueHeight = BOARD_HEIGHT - WIN_SEQUENCE + 1;
        int numOfTests = BOARD_WIDTH * (BOARD_HEIGHT - WIN_SEQUENCE + 1);

        Object[][] objects = new Object[numOfTests][2];
        for (int k = 0; k < trueHeight; k++) {
            for (int i = 0; i < BOARD_WIDTH; i++) {
                int[] columns = new int[WIN_SEQUENCE];
                int[] rows = new int[WIN_SEQUENCE];
                for (int j = 0; j < WIN_SEQUENCE; j++) {
                    rows[j] = k + j;
                    columns[j] = i;
                }
                objects[i + (BOARD_WIDTH * k)][0] = rows;
                objects[i + (BOARD_WIDTH * k)][1] = columns;
            }

        }
        return objects;
    }

    @Test(dataProvider = "verticalWinParamsExtended")
    public void shouldWinVerticallyExtended(int[] rows, int[] columns) throws BoardToSmallException, FieldOccupiedException, CoordinatesOutsideTheBoardException {
        VictoryCheck victoryCheck = new VictoryCheck();
        Board board = new Board(MIN_BOARD_SIZE, MAX_BOARD_SIZE);
        board.setUp(BOARD_HEIGHT, BOARD_WIDTH);
        Player playerX = new Player(String.valueOf(SIGN), SIGN);
        BoardPrinter boardPrinter = new BoardPrinter(board);

        Move lastMove = null;
        for (int j = 0; j < WIN_SEQUENCE; j++) {
            lastMove = board.put(playerX, rows[j], columns[j]);
            System.out.println(boardPrinter.stringOfBoard());
        }
        assertTrue(victoryCheck.isRoundWon(board, lastMove, WIN_SEQUENCE));
    }


}