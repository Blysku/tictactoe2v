package pl.kk.game.io;

import org.testng.annotations.Test;
import pl.kk.game.components.Board;
import pl.kk.game.exceptions.BoardToSmallException;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.testng.Assert.assertEquals;

public class TestConsolePrinter {

    @Test
    public void shouldPrintTextToGivenStream() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Console console = new Console(new PrintStream(byteArrayOutputStream), System.in, new Validator(new Board(1, 100)));
        console.print("it works very well!");

        assertEquals(byteArrayOutputStream.toString(), "it works very well!");

        byteArrayOutputStream = new ByteArrayOutputStream();
        console = new Console(new PrintStream(byteArrayOutputStream), System.in, new Validator(new Board(1, 100)));
        console.println("this works as well!");

        assertEquals(byteArrayOutputStream.toString(), "this works as well!\n");
    }

    @Test
    public void shouldPrintBoardCorrectShape() throws BoardToSmallException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Board board = new Board(1, 1);
        Console console = new Console(new PrintStream(byteArrayOutputStream), System.in, new Validator(board));
        board.setUp(3, 3);
        BoardPrinter boardPrinter = new BoardPrinter(board);
        console.print(boardPrinter.stringOfBoard());
        assertEquals(byteArrayOutputStream.toString(), "+---++---++---+\n" +
                "|   ||   ||   |\n" +
                "+---++---++---+\n" +
                "|   ||   ||   |\n" +
                "+---++---++---+\n" +
                "|   ||   ||   |\n" +
                "+---++---++---+\n"
        );
        byteArrayOutputStream = new ByteArrayOutputStream();
        console = new Console(new PrintStream(byteArrayOutputStream), System.in, new Validator(board));
        board.setUp(5, 5);
        console.print(boardPrinter.stringOfBoard());
        assertEquals(byteArrayOutputStream.toString(), "+---++---++---++---++---+\n" +
                "|   ||   ||   ||   ||   |\n" +
                "+---++---++---++---++---+\n" +
                "|   ||   ||   ||   ||   |\n" +
                "+---++---++---++---++---+\n" +
                "|   ||   ||   ||   ||   |\n" +
                "+---++---++---++---++---+\n" +
                "|   ||   ||   ||   ||   |\n" +
                "+---++---++---++---++---+\n" +
                "|   ||   ||   ||   ||   |\n" +
                "+---++---++---++---++---+\n"
        );
    }
}
