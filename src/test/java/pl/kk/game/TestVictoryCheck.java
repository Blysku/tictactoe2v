package pl.kk.game;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pl.kk.game.components.Board;
import pl.kk.game.components.Move;
import pl.kk.game.components.Player;
import pl.kk.game.io.BoardPrinter;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class TestVictoryCheck {

    @DataProvider(name = "winGame")
    public static Object[][] parametersForWin() {
        return new Object[][]{
                {new Integer[]{4, 3, 2, 1, 0}, new Integer[]{0, 1, 2, 3, 4}, 'X'},
                {new Integer[]{0, 0, 0, 0, 0}, new Integer[]{0, 1, 2, 3, 4}, 'X'},
                {new Integer[]{0, 0, 0, 0, 0}, new Integer[]{0, 1, 4, 3, 2}, 'X'},
                {new Integer[]{0, 0, 0, 0, 0}, new Integer[]{4, 1, 2, 3, 0}, 'X'},
                {new Integer[]{4, 3, 2, 1, 0}, new Integer[]{0, 0, 0, 0, 0}, 'X'},
                {new Integer[]{0, 1, 2, 3, 4}, new Integer[]{0, 1, 2, 3, 4}, 'X'},
        };
    }

    @Test(dataProvider = "winGame")
    public void shouldWinTheGameParametrized(Integer[] xRow, Integer[] xCol, char sign) throws Exception {
        Board board = new Board(1, 100);
        board.setUp(5, 5);
        Player player = new Player(String.valueOf(sign), sign);
        BoardPrinter boardPrinter = new BoardPrinter(board);
        Move move = null;
        for (int i = 0; i < xRow.length; i++) {
            move = board.put(player, xRow[i], xCol[i]);
            System.out.println(boardPrinter.stringOfBoard());
        }
        VictoryCheck victoryCheck = new VictoryCheck();
        assertTrue(victoryCheck.isRoundWon(board, move, 5));
    }

    @DataProvider(name = "drawGame")
    public static Object[][] paramsForDraw() {
        return new Object[][]{
                {new Integer[]{0, 1, 0, 1, 2}, new Integer[]{0, 1, 2, 2, 1}, 'X', new Integer[]{0, 1, 2, 2}, new Integer[]{1, 0, 0, 2}, 'O'},
                {new Integer[]{0, 1, 2, 2}, new Integer[]{0, 2, 0, 1}, 'X', new Integer[]{0, 0, 1, 1, 2}, new Integer[]{1, 2, 0, 1, 2}, 'O'},
        };
    }

    @Test(dataProvider = "drawGame")
    public void shouldBeDraw(Integer[] xRow, Integer[] xCol, char xSign, Integer[] oRow, Integer[] oCol, char oSign) throws Exception {
        Board board = new Board(1, 100);
        board.setUp(3, 3);
        VictoryCheck victoryCheck = new VictoryCheck();
        for (int i = 0; i < xRow.length; i++) {
            board.put(new Player(String.valueOf(xSign), xSign), xRow[i], xCol[i]);
        }
        for (int i = 0; i < oRow.length; i++) {
            board.put(new Player(String.valueOf(oSign), oSign), oRow[i], oCol[i]);
        }
        assertTrue(victoryCheck.isDraw(board));
    }

    @Test
    public void shouldNotWinTheGame() throws Exception {
        Board board = new Board(1, 100);
        board.setUp(3, 3);
        Player player = new Player("X", 'X');
        Move move = board.put(player, 0, 0);
        VictoryCheck victoryCheck = new VictoryCheck();
        assertFalse(victoryCheck.isRoundWon(board, move, 3));
    }


}
